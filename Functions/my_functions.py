#! /usr/bin/python
# -*- coding: utf-8 -*-

import re
import os
import user
import time
import smtplib
import logging
import urllib
import urllib2
import paramiko
import socket
from subprocess import Popen, PIPE
from commands import getoutput

LINUX_USER = "root"
C = {"black": "\033[0;30m",
     "dark_gray": "\033[1;30m",
     "blue": "\033[0;34m",
     "light_blue": "\033[1;34m",
     "green": "\033[0;32m",
     "light_green": "\033[1;32m",
     "cyan": "\033[0;36m",
     "light_cyan": "\033[1;36m",
     "red": "\033[0;31m",
     "light_red": "\033[1;31m",
     "purple": "\033[0;35m",
     "light_purple": "\033[1;35m",
     "brown": "\033[0;33m",
     "yellow": "\033[1;33m",
     "light_gray": "\033[0;37m",
     "white": "\033[1;37m",
     "clear": "\033[0m"}


def scpFileToHost(src_file, username, host, dst_folder, password):
    """
    Description: copy file to remote host using SCP.
        src_file - file to copy to remote host
        username - user to connect to remote host
        host - host to connect to
        dst_folder - destination folder on remote host
        password - password for the host
    """
    # Check if host is responding to ssh
    if not hostAlive(host, username, password):
        print C["white"], host, C["red"], "is down", C["clear"]
        return False

    out = Popen(["scp", src_file, username + "@" + host + ":" + dst_folder],
                stdout=PIPE, stderr=PIPE).communicate()[0]
    des_loc = "".join([host, dst_folder])

    print "copy", C["green"], src_file, C["clear"], "to",\
        C["green"], des_loc, C["clear"]

    return True, out


def autoSSH(host, username, password):
    """
    Description: Get remote host ssh key and add it to local know_hosts file
    ssh-copy-id to remote host to enable ssh connect without password
        host - host to connect to format: user@host
        username - user to connect to remote host
        password - password for the host
        unsecure - use unsecure mode
    """
    home = user.home
    ssh_dir = ".ssh"
    ssh_file = "known_hosts"
    ssh_path = home + "/" + ssh_dir + "/" + ssh_file
    know_host = open(ssh_path, "r").readlines()
    ssh_config = home + "/" + ssh_dir + "/config"
    ssh_config_file_read = open(ssh_config, "r").read()
    ssh_config_file_write = open(ssh_config, "a")

    host_ip = Popen(["host", host], stdout=PIPE).communicate()[0]
    host_ip_addr = host_ip.split()

    # Remove host keys if any exsist
    for line in know_host:
        if host in line:
            Popen(["ssh-keygen", "-R", host, "2>&", "/dev/null"], stdout=PIPE,
                  stderr=PIPE).communicate()
        if host_ip_addr[3] in line:
            Popen(["ssh-keygen", "-R", host_ip_addr[3]], stdout=PIPE,
                  stderr=PIPE).communicate()

    # Scan for the host ssh keys
    host_key = Popen(["ssh-keyscan", "-T", "5", host],
                     stdout=PIPE, stderr=PIPE).communicate()[0]

    if host_key == "":
        err_output = "".join([C["red"], " No ssh keys from %s",
                              C["clear"]])
        print err_output % host
        return False

    # Add host ssh keys to known_hosts file
    else:
        info = " ".join(["Adding ssh key from", C["white"], host,
                        C["clear"], "to known_hosts"])
        print info
        echo_cmd = 'echo " ' + host_key + '" >> ' + ssh_path
        Popen([echo_cmd], stdout=PIPE, shell=True)

        info = " ".join(["Setting password-less log-in to", C["white"],
                         host, C["clear"]])
        print info
        Popen(["sshpass", "-p", password, "ssh-copy-id",
               username + "@" + host], stdout=PIPE, stderr=PIPE).communicate()

    return True


def hostAlive(host, username, password):
    """
    Description: Check if remote host is alive using ssh
        host - host to check
        username - user to connect to remote host
        password - password for the host
    """
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(hostname=host, username=username, password=password,
                    timeout=10)
    except socket.timeout:
        return False
    except socket.gaierror:
        return False
    except socket.error:
        return False
    except paramiko.AuthenticationException:
        return False
    except paramiko.SSHException:
        return False
    return True


def getMacAndIP(interface, host, username, password):
    """
    Description: Get IP and MAC address from given host
        interface - Name of the interface to query
        host - Host to query
        username - user to connect to remote host
        password - password for the host
    """
    # Get interface MAC
    mac_cmd = "".join(["ifconfig " + interface +
                       " | grep HWaddr |awk '{print $5}'"])
    rc, mac_out = actionOnRemoteHost(host, mac_cmd, username, password)
    if not rc:
        return False, ()

    # Get IP
    ip_cmd = "".join(["ifconfig " + interface +
                      " | grep 'inet addr' |cut -d':' -f2|awk '{print $1}'"])
    rc, ip_out = actionOnRemoteHost(host, ip_cmd, username, password)

    if not rc:
        return False, ()

    return True, (mac_out[0], ip_out[0])


def apkRename(apk_path, apk=None):
    """
    Description: Rename apk to valid name.
        apk - apk file to rename
        apk_path - path to apk directory
    """
    # Check if aapt exist om the system
    try:
        Popen(["aapt"], stderr=PIPE).communicate()[0]
    except OSError:
        logging.error("Cannot find aapt binary")
        return False

    # Find all apk files in the diractory and get info from them
    dir_path = "/".join(apk_path)
    ls_out = Popen(["ls", dir_path], stdout=PIPE).communicate()[0]

    for i in ls_out.split("\n"):
        if ".apk" in i:
            cmd = Popen(["aapt", "d", "badging", i],
                        shell=True,
                        stdin=PIPE, stdout=PIPE, close_fds=True)
            fdout = cmd.communicate()[1]
            data = fdout.readlines()

            for line in data:
                if "application: label=" in line:
                    apkname = line.split("'")[1]
                if "pack" in line:
                    apkver = line.split("'")[5]

            newapkname = str(apkname) + "-" + str(apkver) + ".apk"
            newapkpath = apk_path[:-1]
            newapkpath.append(newapkname)
            newapkpathandname = '/'.join(newapkpath)
            os.system("mv " + apk + " " + newapkpathandname)

        return True


def ldapSearchOLD(name):
    """
    Description: Search for user in Redhat corp ldap server
        user - User to search for.
    """
    search_string_uid = 'ldapsearch -x uid=*' + name + '*'
    search_string_cn = 'ldapsearch -x cn=*' + name + '*'

    result = getoutput(search_string_uid)
    if result.find('uid:') == -1:
        result = getoutput(search_string_cn)
        if result.find('cn:') == -1:
            err_output = "".join([C["red"], "User not found",
                                  C["clear"]])
            print C["white"], name, C["clear"], err_output
            return False

    split_result = result.split('\n')

    param_dict = {'dn:': 'dn', 'cn:': 'Full Name:      ', 'uid:': 'IRC:       \
     ', 'mail:': 'Email:          ', 'telephoneNumber:':
                  'Office Ext:     ', 'mobile:': 'Mobile Phone:   ',
                  'rhatLocation:': 'Office Location:', 'rhatCostCenterDesc:':
                  'Job Title:      '}

    for line in split_result:
        for key in param_dict.keys():
            if line.startswith(key):
                if key == 'dn:':
                    print '\n'
                else:
                    new_line = line.split(" ")
                    param = new_line[1:]
                    title = new_line[0].replace(key, param_dict[key])
                    output = "".join([C["light_green"], title,
                                      C["clear"], " ".join(param)])
                    print output
    print '\n'
    return True


def isServiceRunnig(service):
    """
    Description: Check if service is running, retur PID of the service.
        service - service to search for.
    """
    cmd = Popen(["pidof", service], stdout=PIPE)
    out = cmd.communicate()[0]
    list_out = out.split("\n")

    if not out:
        return False

    print "Service %s is running" % (service)
    print "PID: %s" % (list_out[:-1])
    return True


def openvpnConnect(username, password, conf_file):
    """
    Description: Connect to openvpn server
        username - User to connect with
        password - Password to connect with
        conf_file - Configuration file to use
    """
    log_file = "/tmp/openvpn.log"
    pass_file = "/tmp/.vpn"
    tmp_file = open(pass_file, "w")
    tmp_file.write(username + "\n")
    tmp_file.write(password + "\n")
    tmp_file.close()
    index = 0
    dev_cmd = Popen(["grep dev " + conf_file + " | awk '{print $2}'"],
                    shell=True, stdout=PIPE).communicate()[0]
    dev = dev_cmd.split("\n")

    # Check if openvpn service is already running
    if isServiceRunnig("openvpn"):
        print "Already connected to VPN server"
        return False

    # connect to VPN server
    else:
        Popen(["sudo", "openvpn", "--config", conf_file,
               "--auth-user-pass", pass_file, "--daemon",
               "--log", log_file], stdout=PIPE).communicate()
        Popen(["rm", "-rf", pass_file], stdout=None)
        # Check if we got IP on VPN interface (interface name from the conf)
        while index < 30:
            interface = Popen(["ip", "add"], stdout=PIPE)
            intout = interface.communicate()[0]
            for i in dev:
                device = re.search(i, intout)
                if not device:
                    index += 1
                    time.sleep(1)
                else:
                    print "Connected to VPN"
                    return True
        print "Failed to connect to VPN server"
        return False


def actionOnRemoteHost(host, command, username, password, queue=None):
    """
    Description: Run command on remote linux hosts
        host - host to run the remote command.
        command - command to run on remote hosts.
        username - user for ssh connection to remote host
        password - password for the host
    """
    yum_error = "This system is not registered to Red Hat Subscription"
    # Check if host is responding to ssh
    if not hostAlive(host, username, password):
        print C["white"], host, C["red"], "is down", \
            C["clear"]
        return False, None

    # Open ssh connection and run the command
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=host, username=username, password=password)
    stdin, stdout, stderr = ssh.exec_command(command)
    tmp_err = stderr.readlines()

    if tmp_err:
        if yum_error not in tmp_err[0]:
            err = "\n".join(tmp_err)
            error = "".join([C["white"], host + ": ", C["red"],
                             "ERROR", "\n", C["clear"], err])
            print error
            # queue.put(error)
            return False, error

    out = ""
    tmp_out = stdout.readlines()

    for line in tmp_out:
        lstrip = line.strip()
        out += lstrip + "\n"
    output = "".join([C["white"], host + ": ", C["green"],
                      "[" + command + "]", C["clear"], "\n",
                      C["clear"], out])
    print output
    # queue.put(output)
    return True, out


def findInList(list1=list(), list2=list()):
    """
    Description: Search for items from list1 in list2, return status and new
    list. status 0 mean that the new list is not empty and status 1 mean that
    the new list is empty.
        list1 - list to search from.
        list2 - list to search in.
    """
    new_list = []
    status = 1

    for val in list1:
        if val in list2:
            new_list.append(val)
            status = 0
        else:
            print "%s not found" % (val)

    return status, new_list


def sendEmail(server, msg, mail_from, mail_to, server_port=None):
    """
    Basic function to send email
        server - SMTP server
        msg - the email to send
        mail_from - mail from address
        mail_to - mail to address
        server_port - SMTP server port if needed
    """
    if server_port:
        server = smtplib.SMTP(server, server_port)
        if not server:
            print "Failed to connect to %s on port %s" % (server, server_port)
            return False

    server = smtplib.SMTP(server)
    if not server:
        print "Failed to connect to SMTP server %s" % server
        return False

    server.sendmail(mail_from, mail_to, msg)
    print "Sending email to %s" % mail_to
    return True


def ldap_user_search(server, username, domain, port=389):
    try:
        import ldap
    except ImportError:
        logging.error("python-ldap is not installed")
        return False

    result_data = None
    cn_only = False
    full_list = []
    search_scope = ldap.SCOPE_SUBTREE
    ldap_server = "".join(["ldap://", server, ":", str(port)])
    ldap_connection = ldap.initialize(uri=ldap_server)
    base_domain = domain.split(".")
    base_dn = "".join(["dc=", base_domain[0], ",dc=", base_domain[1]])
    retrieve_attributes = ["dn", "cn", "uid", "mail", "telephoneNumber",
                           "mobile", "rhatLocation", "rhatCostCenterDesc",
                           "manager", "rhatPhoneExt", "title", "rhatHireDate"]

    param_dict = {'dn:': 'dn',
                  'cn:': 'Full Name:       ',
                  'uid:': 'IRC:             ',
                  'mail:': 'Email:           ',
                  'telephoneNumber:': 'Telephone Number:',
                  'mobile:': 'Mobile Phone:    ',
                  'rhatLocation:': 'Office Location: ',
                  'rhatCostCenterDesc:': 'Department:      ',
                  'rhatPhoneExt:': 'Office Ext:      ',
                  'manager:': 'Manager:         ',
                  'title:': 'Title:           ',
                  'rhatHireDate:': 'Hire Date        '}

    match = False

    for i in ('uid', 'cn'):
        ldap_result_id = ldap_connection.search(
            base=base_dn, scope=search_scope, filterstr="".join(
                [i, "=*", username, "*"]), attrlist=retrieve_attributes
        )
        result_ldap_data = ldap_connection.result(ldap_result_id, 1)[1]
        if result_ldap_data:
            result_data = result_ldap_data
            break

    if not result_data:
        return False

    if len(result_data) > 5:
        cn_only = True

    for res in result_data:
        new_dict = {}
        if len(res) > 0:
            res = res[1]
            for key in res.keys():
                new_key = key.replace(key, param_dict[key + ":"])
                manager_value = None
                hire_date_value = None

                if cn_only:
                    if key != "cn":
                        continue
                    else:
                        new_dict[new_key] = res[key][0]
                        match = True
                        break

                if key == "rhatHireDate":
                    date = res[key][0][0:8]
                    year, month, day = date[0:4], date[4:6], date[6:8]
                    hire_date_value = "-".join([day, month, year])

                if key == "manager":
                    manager_uid = res[key][0].split(",")[0].split("=")[1]
                    res_id = ldap_connection.search(
                        base=base_dn, scope=search_scope, filterstr="".join(
                            ['uid', "=*", manager_uid, "*"]), attrlist=['cn']
                    )
                    res_data = ldap_connection.result(res_id, 0)[1]
                    manager_value = res_data[0][1]['cn'][0]

                if manager_value is not None:
                    value = manager_value
                elif hire_date_value is not None:
                    value = hire_date_value
                else:
                    value = res[key][0]

                new_dict[new_key] = value
                match = True
            full_list.append(new_dict)

    if not match:
        return False

    if cn_only:
        cn_only_list = []
        for cn_dict in full_list:
            cn_only_list.extend(cn_dict.values())
        return cn_only_list

    return full_list


def acpi_battery(level_only=False):
    batt = "{}Fail{}".format(C["red"], C["clear"])
    acpi = Popen(["acpi"], stdout=PIPE, stderr=PIPE)
    out, err = acpi.communicate()

    if err:
        return batt

    charge = ""
    acpi_list = out.split(" ")

    if "Charging," in acpi_list:
        charge = "⌁"

    for val in acpi_list:
        if "%" in val:
            val = val.strip(",")
            bat_level = val.split(",")[0].strip("%")

            if level_only:
                return val.strip()

            elif int(bat_level) > 50:
                batt = "({}{}{}{})".format(C["green"], charge, val, C["clear"])
                break

            elif int(bat_level) > 20:
                batt = "({}{}{}{})".format(
                    C["yellow"], charge, val, C["clear"]
                )
                break

            else:
                batt = "({}{}{}{})".format(C["red"], charge, val, C["clear"])
                break
    return batt


def rsync_backup(local, remote, log_dir, folders=list(), two_way=False):
    """
    Description: Sync local folders to remote machine using rsync
        local - Local path of folders to sync
        remote - Remote path of folders to sync too
        log_dir - log dir for rsync
        folders - list on folders to sync
        two_way - if true sync from remote to local too
    """
    if isServiceRunnig("/usr/bin/rsync"):
        print "Cannot run backup, another backup is already running"
        return False

    for folder in folders:
        print C["white"], "Backup " + folder, C["clear"]
        print C["green"], "Local TO Remote", C["clear"]
        cmd1 = Popen(["rsync",
                     "--log-file=" + log_dir + "rsync-" + folder + ".log",
                     "--size-only",
                     "--progress",
                     "--human-readable",
                     "-avzrut",
                     local + folder,
                     remote], stdout=PIPE)
        out1, err1 = cmd1.communicate()
        print out1

        if two_way:
            print C["green"], "Remote TO Local", C["clear"]
            cmd2 = Popen(["rsync",
                         "--log-file=" + log_dir + "rsync-" + folder + ".log",
                         "--size-only",
                         "--progress",
                         "--human-readable",
                         "-avzrut",
                         remote + folder,
                         local], stdout=PIPE)
            out2, err2 = cmd2.communicate()
            print out2


def send_notify_to_android(app_token, user_key, msg):
    """
    Description: send notifiction to Android device using https://pushover.net/
    API
        app_token - Token of the app, must be generate on pushover.net
        user_key - user api key from pushover.net
        msg - message to send
    """
    url = "api.pushover.net"
    post_path = "/1/messages.json"
    conn = urllib2.httplib.HTTPSConnection(url, port=443)
    params = urllib.urlencode({"token": app_token,
                               "user": user_key,
                               "message": msg})
    conn.request("POST", post_path, params)
