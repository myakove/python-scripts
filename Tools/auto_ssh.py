#! /usr/bin/python
# -*- coding: utf-8 -*-

from Functions.my_functions import autoSSH
from multiprocessing import Process
import argparse
import logging
import time


class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawTextHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
    """
    Format argparse output
    """
    pass

PARSER = argparse.ArgumentParser(formatter_class=CustomFormatter,
                                 prog="Auto SSH",
                                 usage="%(prog)s [options]")
GROUP_REQUIRED = PARSER.add_argument_group("required arguments")
GROUP_OPTIONAL = PARSER.add_argument_group("optional arguments")

GROUP_OPTIONAL.add_argument("-H", help="Host to connect to (host name if "
                                       "using range host name is without the "
                                       "number)")
GROUP_OPTIONAL.add_argument("-D", help="Domain for the host")
GROUP_OPTIONAL.add_argument("-R", help="To run on range of hosts, "
                                       "example: 1-10 or 01-10")
GROUP_OPTIONAL.add_argument("-U", default="root", help="user to connect to "
                                                       "the host")
GROUP_REQUIRED.add_argument("-P", help="password to connect to the host",
                            required=True)
GROUP_OPTIONAL.add_argument("-F", help="File with hosts list, one host per "
                                       "line, don't use host, domain and "
                                       "range when using --file option")
GROUP_OPTIONAL.add_argument("-C", help="configure auto ssh to one host and "
                                       "connect to it", nargs='?', const=True)
ARGS = PARSER.parse_args()


def sshHostRange(host_range=list()):
    """
    Get host range and run autoSSH function
    """
    range_jobs = []
    for i in range(int(host_range[0]), int(host_range[1]) + 1):
        if i >= 10:
            idx = i
        else:
            idx = "0%d" % i

        active_host = "".join([ARGS.H, str(idx), ".", ARGS.D])
        process = Process(target=autoSSH, args=(active_host, ARGS.U, ARGS.P))
        range_jobs.append(process)
        process.start()
        time.sleep(1)

    for j in range_jobs:
        j.join()


def validateArgumantsAndRun():
    """
    Validate syntax
    """
    yb = True
    try:
        import yum
        yb = yum.YumBase().isPackageInstalled("sshpass")
    except ImportError:
        logging.info("If this script fails check if sshpass in installed")

    if not yb:
        print "sshpass is not installed"
        return False

    if ARGS.F:
        if (ARGS.H or ARGS.D or ARGS.R):
            print "file can only be sent with --user and --password"
            PARSER.print_help()
            return False

        else:
            hosts_file = open(ARGS.F).readlines()
            jobs = []
            for host in hosts_file:
                ssh_host = host.strip()
                process = Process(target=autoSSH, args=(ssh_host, ARGS.U,
                                                        ARGS.P))
                jobs.append(process)
                process.start()
                time.sleep(1)

            for j in jobs:
                j.join()

            return True

    if not ARGS.H:
        print "Host or connect must be specify"
        PARSER.print_help()
        return False

    if ARGS.R:
        host_range = ARGS.R.split("-")
        sshHostRange(host_range)
        return True

    if ARGS.H:
        autoSSH(ARGS.H, ARGS.U, ARGS.P, ARGS.C)
        return True

if __name__ == '__main__':
    validateArgumantsAndRun()
