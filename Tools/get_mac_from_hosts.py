#! /bin/python
# -*- coding: utf-8 -*-

from Functions.my_functions import getMacAndIP, C
import argparse


class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawTextHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
    """
    Format argparse output
    """
    pass

PARSER = argparse.ArgumentParser(formatter_class=CustomFormatter,
                                 prog="Get MAC",
                                 usage="%(prog)s [options]")
GROUP_REQUIRED = PARSER.add_argument_group("required arguments")
GROUP_OPTIONAL = PARSER.add_argument_group("optional arguments")

GROUP_REQUIRED.add_argument("-I", help="Interface to query", required=True)
GROUP_REQUIRED.add_argument("-H", help="File with hosts list, one host per "
                                       "line", required=True)
GROUP_OPTIONAL.add_argument("-U", help="User for remote hosts connections "
                                       "(ssh, default is root)",
                            default="root")
GROUP_OPTIONAL.add_argument("-P", help="ssh password for the host",
                            required=True)
ARGS = PARSER.parse_args()

HOSTS_FILE = open(ARGS.H).readlines()

for host in HOSTS_FILE:
    active_host = host.strip()
    rc, out = getMacAndIP(ARGS.I, active_host, ARGS.U, ARGS.P)
    if not rc:
        continue

    print C["brown"] + active_host, ":", C["clear"]
    print "MAC address:", C["green"], out[0], C["clear"]
    print "IP address :", C["green"], out[1], C["clear"]


