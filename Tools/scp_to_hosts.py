import argparse
import multiprocessing
from Functions.my_functions import scpFileToHost


class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawTextHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
    """
    Format argparse output
    """
    pass

PARSER = argparse.ArgumentParser(formatter_class=CustomFormatter,
                                 prog="SCP to host",
                                 usage="%(prog)s [options]")
GROUP_REQUIRED = PARSER.add_argument_group("required arguments")
GROUP_OPTIONAL = PARSER.add_argument_group("optional arguments")

GROUP_OPTIONAL.add_argument("-H", help="Host to scp to (host name, if using "
                                       "range host name is without the "
                                       "number)")
GROUP_OPTIONAL.add_argument("-D", help="Domain for the host")
GROUP_OPTIONAL.add_argument("-R", help="To run on range of hosts, "
                                       "example: 1-10 or 01-10")
GROUP_OPTIONAL.add_argument("-U", default="root", help="user to connect to "
                                                       "the host")
GROUP_OPTIONAL.add_argument("-F", help="File with hosts list, one host per "
                                        "line, don't use host, domain and "
                                        "range when using --file option")
GROUP_REQUIRED.add_argument("-SF", help="File to scp to remote host",
                            required=True)
GROUP_REQUIRED.add_argument("-DF", help="Destination folder to copy the src_"
                                        "file into", required=True)
GROUP_REQUIRED.add_argument("-P", help="ssh password for the host",
                            required=True)
ARGS = PARSER.parse_args()


def sshHostRange(host_range=list()):
    """
    Get host range and run scpFileToHosts function
    """
    range_jobs = []
    for i in range(int(host_range[0]), int(host_range[1]) + 1):
        if '0' in (host_range[0] and host_range[1]):
            if i >= 10:
                idx = i
            else:
                idx = "0%d" % i
        active_host = "".join([ARGS.H, str(idx), ".", ARGS.D])
        process = multiprocessing.Process(target=scpFileToHost,
                                          args=(ARGS.SF, ARGS.U, active_host,
                                                ARGS.DF, ARGS.P))
        range_jobs.append(process)
        process.start()

    for j in range_jobs:
        j.join()


def validateArgumantsAndRun():
    """
    Validate syntax
    """
    if ARGS.F:
        if (ARGS.H or ARGS.D or ARGS.R):
            print "file can only be sent with --user"
            PARSER.print_help()
            return False

        else:
            hosts_file = open(ARGS.F).readlines()
            jobs = []
            for host in hosts_file:
                ssh_host = host.strip()
                process = multiprocessing.Process(target=scpFileToHost,
                                                  args=(ARGS.SF, ARGS.U,
                                                        ssh_host, ARGS.DF,
                                                        ARGS.P))
                jobs.append(process)
                process.start()

            for j in jobs:
                j.join()

            return True

    if not ARGS.H:
        print "Host must be specify"
        PARSER.print_help()
        return False

    if ARGS.R:
        host_range = ARGS.R.split("-")
        sshHostRange(host_range)
        return True

    if ARGS.H:
        scpFileToHost(ARGS.SF, ARGS.U, ARGS.H, ARGS.DF, ARGS.P)
        return True

if __name__ == '__main__':
    validateArgumantsAndRun()
