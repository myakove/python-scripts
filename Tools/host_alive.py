#! /usr/bin/python
# -*- coding: utf-8 -*-

from Functions.my_functions import hostAlive, C
import argparse


class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawTextHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
    """
    Format argparse output
    """
    pass

PARSER = argparse.ArgumentParser(formatter_class=CustomFormatter,
                                 prog="Check host alive",
                                 usage="%(prog)s [options]")
GROUP_REQUIRED = PARSER.add_argument_group("required arguments")
GROUP_OPTIONAL = PARSER.add_argument_group("optional arguments")

GROUP_REQUIRED.add_argument("-F", help="File with hosts list, one host per "
                                       "line", required=True)
GROUP_OPTIONAL.add_argument("-U", help="ssh user for the host",
                            default="root")
GROUP_REQUIRED.add_argument("-P", help="ssh password for the host",
                            required=True)
ARGS = PARSER.parse_args()

HOSTS_LIST = open(ARGS.F)
HOSTS = [line.strip() for line in HOSTS_LIST]
HOST_LEN = 35

for host in HOSTS:
    SPACE = HOST_LEN - len(host)
    PRINT_HOST_NAME = host + SPACE * " "
    if not hostAlive(host, ARGS.U, ARGS.P):
        OUTPUT = "".join([C["brown"], "%s", C["clear"],
                          " is ", C["red"], "DOWN",
                          C["clear"]]) % PRINT_HOST_NAME
        print OUTPUT
    else:
        OUTPUT = "".join([C["brown"], "%s", C["clear"],
                          " is ", C["green"], "UP",
                          C["clear"]]) % PRINT_HOST_NAME
        print OUTPUT
