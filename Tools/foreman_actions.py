#! /usr/bin/python
# -*- coding: utf-8 -*-

import os
import user
import argparse
import ConfigParser
from Functions.my_functions import C, actionOnRemoteHost
from foreman.client import Foreman


class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawTextHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
    """
    Format argparse output
    """
    pass

CONF_FILE = user.home + "/.foremanAPI.conf"
PARSER = argparse.ArgumentParser(formatter_class=CustomFormatter,
                                 prog="ForemanActions",
                                 usage="%(prog)s [options]", add_help=False)

GROUP = PARSER.add_argument_group("Options")
HELP = PARSER.add_argument_group("Help")
HELP.add_argument("--help", action="help", help='''

$HOME/.foremanAPI.conf conf file is needed.
Example file:

[SETTING]
url=http://foreman.server
username=foreman_username
password=foreman_password
version = foreman_version
host_user=host_username
host_pass=host_password

''')
GROUP.add_argument("-B", required=False,
                   action="store_true", help="Build host")
GROUP.add_argument("-F", help="File with hosts list, one host per line")
GROUP.add_argument("-H", help="Host name")
GROUP.add_argument("-R", required=False, action="store_true",
                   help="Reboot host after build")
GROUP.add_argument("-S", required=False, action="store_true",
                   help="Get host build status")

ARGS = PARSER.parse_args()

CONFIG = ConfigParser.RawConfigParser()
CONFIG.read(CONF_FILE)
HOST_USER = CONFIG.get("SETTING", "host_user")
HOST_PASS = CONFIG.get("SETTING", "host_pass")
HOST_CMD = "reboot"
URL = CONFIG.get("SETTING", "url")
USERNAME = CONFIG.get("SETTING", "username")
PASSWORD = CONFIG.get("SETTING", "password")
VERSION = CONFIG.get("SETTING", "version")
AUTH = (USERNAME, PASSWORD)


def validate_input():
    """
    Validate input from argparse
    """
    if not (ARGS.F or ARGS.H):
        print C["red"], "host or host_file is missing", C["clear"]
        PARSER.print_help()
        return False

    if not (ARGS.B or ARGS.S):
        print C["red"], "At least one argument needed", C["clear"]
        PARSER.print_help()
        return False

    if not os.path.isfile(CONF_FILE):
        print CONF_FILE, "is missing"
        return False
    return True


class ForemanActions(object):
    """
    Foreman actions
    """
    def __init__(self, url, auth, version):
        """
        Create connection to Foreman
        """
        self.url = url
        self.auth = auth
        self.version = version
        self.api = None

    def foreman_connection(self):
        """
        Create connection to Foreman
        """
        self.api = Foreman(url=self.url, auth=self.auth, version=self.version)
        if not self.api:
            print "Failed to connect to Foreman %s" % self.url
            return False

    def build_system(self, host, build=True, reboot=False):
        """
        Build or cancel build system in Foreman
        host - host name to build
        build - True to build False to cancel build
        reboot - Reboot the host after build
        """
        try:
            host_id = self.api.index_hosts(search=host)[0]["host"]["id"]
        except IndexError:
            print "Host", C["white"], host, C["clear"], "not found"
            return False

        if not self.api.update_hosts({"build": build}, host_id):
            print "Failed to build", C["white"], host, C["clear"]
            return False

        status = self.api.show_hosts(host_id)["host"]["build"]
        if status != build:
            print C["white"], host, C["clear"], "build status is",\
                status, "and should be", build
        print C["white"], host, C["clear"], "build status is",\
            C["green"], status, C["clear"]

        if reboot:
            rc, out = actionOnRemoteHost(host, HOST_CMD, HOST_USER, HOST_PASS)
            if not rc:
                print "Failed to reboot", C["white"], host, \
                    C["clear"], "ERROR:", out
                return False

            print C["white"], host, "is rebooting....", C["clear"]
        return True

    def get_build_status(self, host):
        """
        Get status of build for host
        host - host name to get status for
        """
        try:
            host_id = self.api.index_hosts(search=host)[0]["host"]["id"]
        except IndexError:
            print "Host", C["white"], host, C["clear"], "not found"
            return False

        status = self.api.show_hosts(host_id)["host"]["build"]
        if not status:
            print "Failed to get build status for", C["white"],\
                host, C["clear"]
            return False

        print C["white"], host, C["clear"], "build status is",\
            C["green"], status, C["clear"]
        return True

if __name__ == '__main__':
    if validate_input():
        foreman_obj = ForemanActions(url=URL, auth=AUTH, version=VERSION)
        foreman_obj.foreman_connection()
        if ARGS.F:
            for host in open(ARGS.F, "r").readlines():
                host = host.strip()
                if ARGS.S:
                    foreman_obj.get_build_status(host)
                if ARGS.B:
                    foreman_obj.build_system(host, ARGS.B)
        else:
            if ARGS.S:
                foreman_obj.get_build_status(ARGS.H)
            if ARGS.B:
                foreman_obj.build_system(ARGS.H, ARGS.B)


