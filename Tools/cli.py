#! /usr/bin/python
# -*- coding: utf-8 -*-
import ConfigParser
import os
import user
import click
import time
from multiprocessing import Process
from subprocess import Popen, PIPE
from Functions.my_functions import (
    autoSSH, actionOnRemoteHost, scpFileToHost, hostAlive, C,
    ldap_user_search, getMacAndIP
)


@click.group()
def cli():
    """
    Run tools from CLI
    """
    pass


@cli.command()
@click.option("--hosts_file", help="File with hosts list, one host per line",
              required=True)
@click.option("--username", help="ssh user for hosts", required=True)
@click.option("--password", help="ssh password for hosts", required=True)
def host_alive(hosts_file, username, password):
    """
    Check if host is alive (ssh)
    """
    hosts_list = open(hosts_file)
    hosts = [line.strip() for line in hosts_list]
    host_len = 35

    for host in hosts:
        space = host_len - len(host)
        print_host_name = host + space * " "
        if not hostAlive(host, username, password):
            output = "".join([C["brown"], "%s", C["clear"],
                              " is ", C["red"], "DOWN",
                              C["clear"]]) % print_host_name
            click.echo(output)
        else:
            host_ip = Popen(
                ["host", host], stdout=PIPE, stderr=PIPE).communicate()[0]
            output = "".join([C["brown"], "%s", C["clear"],
                              " is ", C["green"], "UP    ",
                              C["clear"], "IP: ",
                              host_ip.split()[-1]]) % print_host_name
            click.echo(output)
    return True


@cli.command()
@click.option("--username", help="User to search for")
def ldap_search(username):
    """
    Search for user in LDAP server

    \b
    conf file with server and domain is required.
    File should be at $HOME/.ldap-search.conf
    Example file:
        [SETTING]
        server=ldap.server
        domain=domain.com
    """
    conf_file = user.home + "/.ldap-search.conf"
    if not os.path.isfile(conf_file):
        print conf_file, "is missing"
        return False
    config = ConfigParser.RawConfigParser()
    config.read(conf_file)
    server = config.get("SETTING", "server")
    domain = config.get("SETTING", "domain")
    new_list = ldap_user_search(server, username, domain)
    if new_list:
        if isinstance(new_list[0], dict):
            print "*******************************************************"
            for new_dict in new_list:
                for key, val in sorted(new_dict.iteritems()):
                    print C["light_green"], key, C["clear"], val
                print "*******************************************************"
            return True

        else:
            print "".join([C["white"], "Found ", str(len(new_list)),
                           " matches for user ", C["green"], username,
                           C["white"], " please choose one.",
                           C["clear"], "\n"])

            for x in new_list:
                match = x.lower().find(str(username))
                user_len = len(username)
                color_user = "".join(
                    [x[:match], C["green"], x[match:match + user_len],
                     C["clear"], x[match + user_len:]]
                )
                print color_user + ",",
            return True
    else:
        err = "".join(["User ", C["red"], username, C["clear"],
                       " not found"])
        print err
        return False


@cli.command()
@click.option("--hosts_file", help="File with hosts list, one host per line",
              required=True)
@click.option("--username", help="ssh user for hosts", required=True)
@click.option("--password", help="ssh password for hosts", required=True)
@click.option("--interface", help="Interface to query", required=True)
def get_mac_and_ip(hosts_file, interface, username, password):
    """
    Get MAC and IP from hosts
    """
    hosts_file = open(hosts_file).readlines()

    for host in hosts_file:
        active_host = host.strip()
        rc, out = getMacAndIP(interface, active_host, username, password)
        if not rc:
            continue

        print C["brown"] + active_host, ":", C["clear"]
        print "MAC address:", C["green"], out[0], C["clear"]
        print "IP address :", C["green"], out[1], C["clear"]


@cli.command()
@click.option("--host", help="Host to connect to (host name if "
                             "using range host name is without the number)")
@click.option("--domain", help="Domain for the host")
@click.option("--host_range", help="To run on range of hosts, example: "
                                   "1-10 or 01-10")
@click.option("--username", help="ssh user for hosts", required=True)
@click.option("--password", help="ssh password for hosts", required=True)
@click.option("--hosts_file", help="File with hosts list, one host per line")
@click.option("--connect", help="configure auto ssh to one host and "
                                "connect to it", is_flag=True)
def auto_ssh(host, domain, username, password, host_range, hosts_file,
             connect):
    """
    Configure auto SSH connect to hosts
    """
    def sshHostRange(host_range=list()):
        range_jobs = []
        for i in range(int(host_range[0]), int(host_range[1]) + 1):
            if i >= 10:
                idx = i
            else:
                idx = "0%d" % i

            active_host = "".join([host, str(idx), ".", domain])
            process = Process(target=autoSSH, args=(active_host, username,
                                                    password))
            range_jobs.append(process)
            process.start()
            time.sleep(1)

        for j in range_jobs:
            j.join()

    yb = True
    try:
        import yum
        yb = yum.YumBase().isPackageInstalled("sshpass")
    except ImportError:
        click.echo("If this script fails check if sshpass in installed")

    if not yb:
        print "sshpass is not installed"
        return False

    if hosts_file:
        if host or domain or host_range:
            print "file can only be sent with --user and --password"
            return False

        else:
            hosts_file = open(hosts_file).readlines()
            jobs = []
            for host in hosts_file:
                ssh_host = host.strip()
                process = Process(target=autoSSH, args=(ssh_host, username,
                                                        password))
                jobs.append(process)
                process.start()
                time.sleep(1)

            for j in jobs:
                j.join()

            return True

    if not host:
        print "Host or connect must be specify"
        return False

    if host_range:
        host_range = host_range.split("-")
        sshHostRange(host_range)
        return True

    if host:
        autoSSH(host, username, password, connect)
        return True


@cli.command()
@click.option("--hosts_file", help="File with hosts list, one host per line",
              required=True)
@click.option("--username", help="ssh user for hosts", required=True)
@click.option("--password", help="ssh password for hosts", required=True)
@click.option("--command", help="Command to run on remote hosts",
              required=True)
def remote_command(hosts_file, username, password, command):
    """
    Run command on remote host
    """
    jobs = []
    hosts = open(hosts_file).readlines()

    for host in hosts:
        ssh_host = host.strip()
        process = Process(target=actionOnRemoteHost, args=(ssh_host,
                                                           command,
                                                           username,
                                                           password))
        jobs.append(process)
        process.start()

    for j in jobs:
        j.join()


@cli.command()
@click.option("--host", help="Host to connect to (host name if "
                             "using range host name is without the number)")
@click.option("--domain", help="Domain for the host")
@click.option("--host_range", help="To run on range of hosts, example: "
                                   "1-10 or 01-10")
@click.option("--username", help="ssh user for hosts", required=True)
@click.option("--password", help="ssh password for hosts", required=True)
@click.option("--hosts_file", help="File with hosts list, one host per line")
@click.option("--src_file", help="File to scp to remote host", required=True)
@click.option("--dst_file", help="Destination folder to copy the src file "
                                 "into", required=True)
def scp_to_host(host, domain, username, password, host_range, hosts_file,
                src_file, dst_file):
    """
    SCP files to remote host
    """
    def sshHostRange(host_range=list()):
        """
        Get host range and run scpFileToHosts function
        """
        range_jobs = []
        for i in range(int(host_range[0]), int(host_range[1]) + 1):
            if '0' in (host_range[0] and host_range[1]):
                if i >= 10:
                    idx = i
                else:
                    idx = "0%d" % i
            active_host = "".join([host, str(idx), ".", domain])
            process = Process(target=scpFileToHost, args=(src_file, username,
                                                          active_host,
                                                          dst_file, password))
            range_jobs.append(process)
            process.start()

        for j in range_jobs:
            j.join()

    if hosts_file:
        if (host or domain or host_range):
            print "file can only be sent with --user"
            return False

        else:
            hosts_file = open(hosts_file).readlines()
            jobs = []
            for host in hosts_file:
                ssh_host = host.strip()
                process = Process(target=scpFileToHost, args=(src_file,
                                                              username,
                                                              ssh_host,
                                                              dst_file,
                                                              password))
                jobs.append(process)
                process.start()

            for j in jobs:
                j.join()

            return True

    if not host:
        print "Host must be specify"
        return False

    if host_range:
        host_range = host_range.split("-")
        sshHostRange(host_range)
        return True

    if host:
        scpFileToHost(src_file, username, host, dst_file, password)
        return True


cli.add_command(host_alive)
cli.add_command(ldap_search)
cli.add_command(get_mac_and_ip)
cli.add_command(auto_ssh)
cli.add_command(remote_command)
cli.add_command(scp_to_host)

if __name__ == '__main__':
    cli()
