#! /bin/python
# -*- coding: utf-8 -*-

from Functions.my_functions import ldap_user_search
import argparse
import ConfigParser
import user
import os

CONF_FILE = user.home + "/.ldap-search.conf"


class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawTextHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
    """
    Format argparse output
    """
    pass

PARSER = argparse.ArgumentParser(formatter_class=CustomFormatter,
                                 prog="LDAP user search",
                                 usage="%(prog)s [options]")
GROUP_REQUIRED = PARSER.add_argument_group("required arguments")
GROUP_OPTIONAL = PARSER.add_argument_group("optional arguments")

GROUP_OPTIONAL.add_argument("-S", help="ldap server")
GROUP_OPTIONAL.add_argument("-D", help="domain of the ldap server")
GROUP_REQUIRED.add_argument("-U", help="User to search for.", required=True)
GROUP_OPTIONAL.add_argument("-F", help='''
conf file with server and domain. File should be at $HOME/.ldap-search.conf
Example file:
             [SETTING]
             server=ldap.server
             domain=domain.com''', action="store_false")
ARGS = PARSER.parse_args()


def validateInput():
    if not (ARGS.S and ARGS.D):
        if ARGS.F:
            if not os.path.isfile(CONF_FILE):
                print CONF_FILE, "is missing"
                return False
            config = ConfigParser.RawConfigParser()
            config.read(CONF_FILE)
            server = config.get("SETTING", "server")
            domain = config.get("SETTING", "domain")
            ldap_user_search(server, ARGS.U, domain)
            return True
        print "Server and domain or file must be specify"
        return False
    else:
        server = ARGS.S
        domain = ARGS.D
        ldap_user_search(server, ARGS.U, domain)
    return True

if __name__ == '__main__':
    validateInput()
