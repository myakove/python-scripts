#! /usr/bin/python
# -*- coding: utf-8 -*-

from Functions.my_functions import actionOnRemoteHost
import argparse
from multiprocessing import Process


class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawTextHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
    """
    Format argparse output
    """
    pass

PARSER = argparse.ArgumentParser(formatter_class=CustomFormatter,
                                 prog="Command on remote host",
                                 usage="%(prog)s [options]")
GROUP_REQUIRED = PARSER.add_argument_group("required arguments")
GROUP_OPTIONAL = PARSER.add_argument_group("optional arguments")

GROUP_REQUIRED.add_argument("-H", help="hosts file, one host per line",
                            required=True)
GROUP_REQUIRED.add_argument("-C", help="Command to run on remote hosts",
                            required=True)
GROUP_OPTIONAL.add_argument("-U", help="User for SSH connections",
                                       default="root")
GROUP_REQUIRED.add_argument("-P", help="ssh password for the host",
                            required=True)
ARGS = PARSER.parse_args()

JOBS = []
HOSTS = open(ARGS.H).readlines()

for host in HOSTS:
    ssh_host = host.strip()
    PROCESS = Process(target=actionOnRemoteHost, args=(ssh_host, ARGS.C,
                                                       ARGS.U, ARGS.P))
    JOBS.append(PROCESS)
    PROCESS.start()

for j in JOBS:
    j.join()
