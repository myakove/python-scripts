#! /usr/bin/python

from Functions.my_functions import apkRename
import argparse

USER_INPUT = argparse.ArgumentParser(formatter_class=
                                     argparse.RawTextHelpFormatter)
USER_INPUT.add_argument('--path', '-P', help="path to the APK file",
                        required=True)
USER_INPUT.add_argument('--file', '-F', help="APK file to rename",
                        required=False)
OPTION = USER_INPUT.parse_args()

APK_PATH = OPTION.path.split('/')
APK = OPTION.file if OPTION.file else None

apkRename(APK_PATH, APK)
