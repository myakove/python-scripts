#! /usr/bin/python

import sys
import user
import ConfigParser

import pexpect


SSH_PASS_FILE = user.home + "/.ssh_pass"
config = ConfigParser.RawConfigParser()
config.read(SSH_PASS_FILE)
argv = sys.argv[1].split("@")

child = pexpect.spawn("ssh " + argv[0] + "@" + argv[1])
child.expect("yes/no.*")
child.sendline("yes")
child.interact()

#cmd_line = "".join(["ssh ", argv[0] + "@" + argv[1]])
#Popen([cmd_line], shell=True).communicate()


#autoSSH(host=argv[1], username=argv[0],
#        password=config.get("PASSWORD", "pass"), connect=True)
