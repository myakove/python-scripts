__author__ = 'myakove'


import netaddr
import netifaces


primary_interface = None
secondary_interface = None

all_interfaces = netifaces.interfaces()
for interface in all_interfaces:
    interface_info = netifaces.ifaddresses(interface)
    if interface == "lo":
        continue

    if interface_info.get(2):
        if netaddr.valid_ipv4(interface_info[2][0]["addr"]):
            primary_interface = interface
            primary_interface_mac = interface_info[17][0]["addr"]
            primary_interface_mac_split = primary_interface_mac.split(":")[:-1]
            break

for interface in all_interfaces:
    if interface == primary_interface:
        continue

    interface_info = netifaces.ifaddresses(interface)
    interface_mac = interface_info[17][0]["addr"]
    split_mac = interface_mac.split(":")[:-1]
    if split_mac == primary_interface_mac_split:
        secondary_interface = interface
        break

print primary_interface
print secondary_interface


